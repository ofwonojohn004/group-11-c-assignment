
## Student information management system
## Main Fields
- Name
- registration number
- date of birth
- annual tuition


## Data structure used
- Array

## Considerations for selecting array data structure
- Fast acces, they provide fast access to elements using indices
- Sorting and Searching, arrays can easily be stored and searched using various algorithms
- Simple implementaton, they have  simple implementation making them easy to understand
- Memory efficiency, arrays store elements continuosly making them suitable for large datasets


# Group Members
- Nuweabimpa Isaiah   2300726082 (Youtube https://youtu.be/KrBdWKaizno)
- Ofwono John Paul    2300716550 (Youtube link https://youtu.be/MBIyNz-1eKw)
- Gyoga Derrick       2300727184
- Kamuli Bob          2300727561 (Youtube https://youtu.be/RynouuUnS4E)
