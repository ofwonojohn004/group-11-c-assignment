#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 50 // Including null terminator
#define DATE_LENGTH 10 // YYYY-MM-DD plus null terminator
#define REGISTRATION_NUMBER_LENGTH 6 // 11 digits plus null terminator
#define PROGRAM_CODE_LENGTH 5 // 4 characters plus null terminator
#define MAX_STUDENTS 100

typedef struct {
    char name[MAX_NAME_LENGTH];
    char date_of_birth[DATE_LENGTH];
    char registration_number[REGISTRATION_NUMBER_LENGTH];
    char program_code[PROGRAM_CODE_LENGTH];
    float annual_tuition;
} Student;

// Function prototypes
void add_student(Student *students, int *num_students);
void display_students(Student *students, int num_students);
void update_student(Student *students, int num_students);
void delete_student(Student *students, int *num_students);
void search_student(Student *students, int num_students);
void sort_students(Student *students, int num_students);
void export_to_file(Student *students, int num_students);

// Function to swap two student records
void swap(Student *a, Student *b) {
    Student temp = *a;
    *a = *b;
    *b = temp;
}

// Function to sort students based on name using selection sort
void selection_sort_name(Student *students, int num_students) {
    int i, j, min_idx;
    for (i = 0; i < num_students - 1; i++) {
        min_idx = i;
        for (j = i + 1; j < num_students; j++) {
            if (strcmp(students[j].name, students[min_idx].name) < 0) {
                min_idx = j;
            }
        }
        if (min_idx != i) {
            swap(&students[i], &students[min_idx]);
        }
    }
}

// Function to sort students based on registration number using selection sort
void selection_sort_registration_number(Student *students, int num_students) {
    int i, j, min_idx;
    for (i = 0; i < num_students - 1; i++) {
        min_idx = i;
        for (j = i + 1; j < num_students; j++) {
            if (strcmp(students[j].registration_number, students[min_idx].registration_number) < 0) {
                min_idx = j;
            }
        }
        if (min_idx != i) {
            swap(&students[i], &students[min_idx]);
        }
    }
}

int main() {
    Student students[MAX_STUDENTS];
    int num_students = 0;
    int choice;
    
    do {
        printf("\nMenu:\n");
        printf("1. Add Student\n");
        printf("2. Display Students\n");
        printf("3. Update Student\n");
        printf("4. Delete Student\n");
        printf("5. Search Student\n");
        printf("6. Sort Students\n");
        printf("7. Export Records to File\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        
        switch(choice) {
            case 1:
                add_student(students, &num_students);
                break;
            case 2:
                display_students(students, num_students);
                break;
            case 3:
                update_student(students, num_students);
                break;
            case 4:
                delete_student(students, &num_students);
                break;
            case 5:
                search_student(students, num_students);
                break;
            case 6:
                sort_students(students, num_students);
                break;
            case 7:
                export_to_file(students, num_students);
                break;
            case 8:
                printf("Exiting...\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    } while(choice != 8);
    
    return 0;
}

// Function to add a new student
void add_student(Student *students, int *num_students) {
    if (*num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }
    
    Student new_student;
    printf("Enter name: ");
    scanf(" %[^\n]s", new_student.name);
    printf("Enter date of birth (YYYY-MM-DD): ");
    scanf("%s", new_student.date_of_birth);
    printf("Enter registration number: ");
    scanf("%s", new_student.registration_number);
    printf("Enter program code: ");
    scanf("%s", new_student.program_code);
    printf("Enter annual tuition: ");
    scanf("%f", &new_student.annual_tuition);
    
    students[*num_students] = new_student;
    (*num_students)++;
    
    printf("\nStudent added successfully.\n");
}

// Function to display all students
void display_students(Student *students, int num_students) {
    if (num_students == 0) {
        printf("No students to display.\n");
        return;
    }
    
    printf("List of students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].date_of_birth);
        printf("Registration Number: %s\n", students[i].registration_number);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: Shs%.2f\n", students[i].annual_tuition);
        printf("\n");
    }
}

// Function to update a student's information
void update_student(Student *students, int num_students) {
    if (num_students == 0) {
        printf("No students to update.\n");
        return;
    }
    
    char reg_num[REGISTRATION_NUMBER_LENGTH];
    printf("Enter the registration number of the student to update: ");
    scanf("%s", reg_num);
    
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration_number, reg_num) == 0) {
            printf("Enter new name: ");
            scanf(" %[^\n]s", students[i].name);
            printf("Enter new date of birth (YYYY-MM-DD): ");
            scanf("%s", students[i].date_of_birth);
            printf("Enter new registration number: ");
            scanf("%s", students[i].registration_number);
            printf("Enter new program code: ");
            scanf("%s", students[i].program_code);
            printf("Enter new annual tuition: ");
            scanf("%f", &students[i].annual_tuition);
            printf("Student updated successfully.\n");
            return;
        }
    }
    
    printf("Student with registration number %s not found.\n", reg_num);
}

// Function to delete a student
void delete_student(Student *students, int *num_students) {
    if (*num_students == 0) {
        printf("No students to delete.\n");
        return;
    }
    
    char reg_num[REGISTRATION_NUMBER_LENGTH];
    printf("Enter the registration number of the student to delete: ");
    scanf("%s", reg_num);
    
    for (int i = 0; i < *num_students; i++) {
        if (strcmp(students[i].registration_number, reg_num) == 0) {
            for (int j = i; j < *num_students - 1; j++) {
                students[j] = students[j + 1];
            }
            (*num_students)--;
            printf("Student deleted successfully.\n");
            return;
        }
    }
    
    printf("Student with registration number %s not found.\n", reg_num);
}

// Function to search for a student by registration number
void search_student(Student *students, int num_students) {
    if (num_students == 0) {
        printf("No students to search.\n");
        return;
    }
    
    char reg_num[REGISTRATION_NUMBER_LENGTH];
    printf("Enter the registration number of the student to search: ");
    scanf("%s", reg_num);
    
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration_number, reg_num) == 0) {
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].date_of_birth);
            printf("Registration Number: %s\n", students[i].registration_number);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition: Shs%.2f\n", students[i].annual_tuition);
            return;
        }
    }
    
    printf("Student with registration number %s not found.\n", reg_num);
}

// Function to sort students based on specified fields
void sort_students(Student *students, int num_students) {
    int choice;
    printf("Select sorting criteria:\n");
    printf("1. Sort by name\n");
    printf("2. Sort by registration number\n");
    printf("Enter your choice: ");
    scanf("%d", &choice);

    switch (choice) {
        case 1:
            selection_sort_name(students, num_students);
            printf("Students sorted by name.\n");
            break;
        case 2:
            selection_sort_registration_number(students, num_students);
            printf("Students sorted by registration number.\n");
            break;
        default:
            printf("Invalid choice.\n");
            break;
    }
}

// Function to export records to a file
void export_to_file(Student *students, int num_students) {
    FILE *file = fopen("student_records.csv", "a");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }
    
    for (int i = 0; i < num_students; i++) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].date_of_birth, 
                students[i].registration_number, students[i].program_code, 
                students[i].annual_tuition);
    }
    
    fclose(file);
    printf("Records exported to file successfully.\n");
}
